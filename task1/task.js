const tasks = [
    {id: 234, title: 'Create user registration API', timeSpent: 4, category: 'Backend', type: 'task'},
    {id: 235, title: 'Create user registration UI', timeSpent: 8, category: 'Frontend', type: 'task'},
    {id: 237, title: 'User sign-in via Google UI', timeSpent: 3.5, category: 'Frontend', type: 'task'},
    {id: 238, title: 'User sign-in via Google API', timeSpent: 5, category: 'Backend', type: 'task'},
    {id: 241, title: 'Fix account linking', timeSpent: 5, category: 'Backend', type: 'bug'},
    {id: 250, title: 'Fix wrong time created on new record', timeSpent: 1, category: 'Backend', type: 'bug'},
    {id: 262, title: 'Fix sign-in failed messages', timeSpent: 2, category: 'Frontend', type: 'bug'},
];

const currentFrontedTime = tasks.filter(obj => obj.category === "Frontend").map(value => value.timeSpent).reduce((previousValue, currentValue) => previousValue + currentValue);
const currentBugTime = tasks.filter(obj => obj.type === "bug").map(value => value.timeSpent).reduce((previousValue, currentValue) => previousValue + currentValue);
const lengthUITask = tasks.filter(obj => obj.title.toUpperCase().includes('UI')).length;
const countTaskCategory = tasks.reduce((previousValue, currentValue) => {
    previousValue[currentValue.category] = previousValue[currentValue.category] === undefined ? 1 : previousValue[currentValue.category] + 1;
    return previousValue;
}, {});
const taskBigTime = tasks.filter(obj => obj.timeSpent > 4).map(value => {
    return {title: value.title, category: value.category};
});

console.log("Общее количество времени, затраченное на работу над задачами из категории 'Frontend' - " + currentFrontedTime);
console.log("Общее количество времени, затраченное на работу над задачами типа 'bug' - " + currentBugTime);
console.log("Количество задач, имеющих в названии слово \"UI\" - " + lengthUITask);
console.log(countTaskCategory);
console.log(taskBigTime);