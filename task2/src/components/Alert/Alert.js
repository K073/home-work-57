import React from 'react';

const Alert = props => {
  const state = props.alert ? <div>{props.msg}</div> : null;
  return state;
};

export default Alert;