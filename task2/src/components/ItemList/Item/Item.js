import React from 'react';

const Item = props => {
    return (
        <div>
            <span>{props.name}</span>
            <span>{props.cost}</span>
            <button onClick={props.click}>X</button>
        </div>
    );
};


export default Item;