import React from 'react';
import Item from "./Item/Item";

const ItemList = props => {
    let cost = 0;

    const itemList = props.list.map((value, index) => {
        cost += parseInt(value.cost);
        return <Item key={index} name={value.name} cost={value.cost} click={() => props.remove(index)} />
    });

    return (
        <div>
            {itemList}
            <span>Full cost - {cost}</span>
        </div>
    );
}

export default ItemList;