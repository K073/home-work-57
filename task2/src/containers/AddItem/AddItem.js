import React, {Component} from 'react';
import Alert from "../../components/Alert/Alert";

class AddItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      cost: 0,
      msg: "Заполните все поля"
    };
  }

  handleInputChange = event => {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
    this.setState({invalid: false})
  };

  handlerSubmit = event => {
    if (this.state.name !== '' && this.state.cost !== 0) {
      this.props.click(this.state.name, this.state.cost);
      this.setState({name: '', cost: 0});
      this.setState({invalid: false})
    } else {
      this.setState({invalid: true})
    }
    event.preventDefault();
  };

  render() {
    return (
      <form ref="form" onSubmit={this.handlerSubmit}>
        <input type="text" placeholder="item name" name="name" value={this.state.name}
               onChange={this.handleInputChange}/>
        <input type="number" placeholder="cost" name="cost" value={this.state.cost}
               onChange={this.handleInputChange}/>
        <span>KGS</span>
        <button type="submit">add</button>
        <Alert alert={this.state.invalid} msg={this.state.msg}/>
      </form>
    );
  };
}

export default AddItem;