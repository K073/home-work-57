import React, {Component} from 'react';
import './App.css';
import AddItem from "../AddItem/AddItem";
import ItemList from "../../components/ItemList/ItemList";

class App extends Component {
    state = {
        itemList: []
    };

    addItem = (name, cost) => {
        const item = {};
        item.name = name;
        item.cost = cost;

        const itemList = [...this.state.itemList];
        itemList.push(item);
        this.setState({itemList});
    };

    removeItem = index => {
        const itemList = [...this.state.itemList];
        itemList.splice(index, 1);

        this.setState({itemList});
    };

    render() {
        return (
            <div className="App">
                <AddItem click={this.addItem}/>
                <ItemList list={this.state.itemList} remove={this.removeItem}/>
            </div>
        );
    }
}

export default App;
